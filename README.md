# Initialize Servers

This ansible project can initialize your servers and install selected packages.

## Copy ssh keys

1. Copy `inventory/sampl` to `inventory/myinventory`
2. Edit `hosts.ini`, `group_vars/all.yml` to meet your needs
3. Run `bash copy-keys.sh`

## Configure Servers

1. Run `bash configure-servers.sh`

## Full Upgrade all nodes

1. Run `bash upgrade-full.sh`

## Reboot

1. Run `reboot.sh`
2. All your nodes will be rebooted
