#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

rm -rf ansible.log
ansible-playbook -i inventory/myinventory/hosts.ini configure-servers.yml -vvv
