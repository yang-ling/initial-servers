Copy Keys
=========

Copy SSH keys to servers

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: copy-keys }

License
-------

BSD

Author Information
------------------

SpicyCat
